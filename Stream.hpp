//
// Created by Matthias Wittgen on 11/7/23.
//
#pragma once
#include <cstdint>
#include <vector>
#include "HitMap.hpp"
struct Stream {
    uint32_t readBytes(uint8_t *bytes, uint32_t pos, bool &value) {
        value = 0;
        //We do bit bye bit, it's lazy but safe
        //Bit possitions of 0(1,3) are not allowed in 64 bit blocks due to the structure of data
        if (pos % 64 == 0) pos++;
        if (m_enable_cid && pos % 64 == 1) pos++;
        if (m_enable_cid && pos % 64 == 2) pos++;
        value |= uint32_t((bytes[pos / 8] >> (7 - (pos % 8))) & 0x1);
        pos++; //We seperate index and pos as certain positions are not allowed
        return pos;
    }

    uint32_t readBytes(uint8_t *bytes, uint32_t pos, uint32_t &value, uint32_t len) {

        static const uint8_t LUT[9] = {0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F, 0x7F, 0xFF};

        value = 0;
        uint8_t index = 0; // We need keep a seperate  index and pos as certain positions are reserved and not used
        while (index < len) {
            if (pos % 64 == 0) pos++;
            if (m_enable_cid && pos % 64 == 1) pos++;
            if (m_enable_cid && pos % 64 == 2) pos++;

            uint32_t posMax = ((len - index) > (8 - (pos % 8))) ? (8 - pos % 8) : (len - index);

            value |= uint32_t(((bytes[pos / 8] >> (8 - (posMax + pos % 8))) & LUT[posMax]) << (len - (index + posMax)));
            //std::cout<<" posMax "<<posMax<<" bytes "<<std::hex<<int(bytes[pos/8])<<std::dec<<" val  "<<value<<" ";
            index += posMax;
            pos += posMax;
            //std::cout<<" index "<<int(index)<<" pos "<<pos<<std::endl;
        }
        //std::cout<<" - pos new - "<<pos<<" -- value --"<<std::hex<<value<<std::dec<<std::endl;
        return pos;

    }


    uint32_t readBytes(uint8_t *bytes, uint32_t pos, uint64_t &value, uint32_t len) {

        static uint8_t LUT[9] = {0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F, 0x7F, 0xFF};
        value = 0;
        uint8_t index = 0; // We need keep a seperate  index and pos as certain positions are reserved and not used
        //std::cout<<m_enable_cid<<" - pos - "<<pos<<" - len - "<<len<<std::endl;;
        while (index < len) {
            if (pos % 64 == 0) pos++;
            if (m_enable_cid && pos % 64 == 1) pos++;
            if (m_enable_cid && pos % 64 == 2) pos++;
            uint32_t posMax = ((len - index) > (8 - (pos % 8))) ? (8 - pos % 8) : (len - index);
            value |= uint64_t((bytes[pos / 8] >> (8 - (posMax + pos % 8))) & LUT[posMax]) << (len - (index + posMax));
            index += posMax;
            pos += posMax;
        }
        return pos;

    }

    uint32_t writeBytes(uint8_t *bytes, uint32_t pos, uint64_t value, uint32_t len) {
        //We do bit by bit, it's lazy but safe
        for (uint32_t index = 0; index < len; index++) {
            if (pos % 64 == 0) { for (uint32_t i = 0; i < 8; i++) { bytes[pos / 8 + i] = 0; }} //clean the next 8 bytes
            if (pos % 64 == 0) pos++;
            if (m_enable_cid && pos % 64 == 1) pos++;
            if (m_enable_cid && pos % 64 == 2) pos++;
            bytes[pos / 8] |= ((value >> (len - index - 1)) & 0x1) << (7 - (pos % 8));
            pos++; //We seperate index and pos as certain positions are not allowed
        }
        return pos;
    }

    uint32_t writeBytes(uint8_t *bytes, uint32_t pos, bool value) {
        return writeBytes(bytes, pos, (uint64_t) value, 1);
    }

    uint32_t Pack(uint8_t *bytes) {
        bool isfirst = 1;
        bool islast = 1;
        bool isneighbor = 0;
        uint32_t tagsize = 8;
        uint32_t colsize = 6;
        uint32_t rowsize = 8;
        uint32_t pos = 0;
        uint32_t map = 0;
        uint32_t len = 0;

        for (uint32_t hit = 0; hit < m_tags.size(); hit++) {
            pos = writeBytes(bytes, pos, m_tags.at(hit), tagsize);
            pos = writeBytes(bytes, pos, m_qcols.at(hit), colsize);

            //calculate islast and isneighbor flags
            //islast is set if this is the last qrow in the ccol and zero otherwise
            //isneighbor is set if the previous address was qrow-1 and zero otherwise
            if (hit < m_tags.size() - 1) {
                if (m_qcols.at(hit + 1) == m_qcols.at(hit)) { islast = 0; } else { islast = 1; }
                if (m_qcols.at(hit + 1) == m_qcols.at(hit) and
                    m_qrows.at(hit + 1) == m_qrows.at(hit) + 1) { isneighbor = 1; }
            } else {
                islast = 1;
                isneighbor = 0;
            }

            //is-last
            pos = writeBytes(bytes, pos, islast);

            //is-neighbor
            pos = writeBytes(bytes, pos, isneighbor);
            //row
            pos = writeBytes(bytes, pos, m_qrows.at(hit), rowsize);
            //hit-map
            if (m_enable_enc) {
                len = HitMap::encode(m_hitmaps.at(hit), map);
                pos = writeBytes(bytes, pos, map >> (30 - len), len);
            } else {
                pos = writeBytes(bytes, pos, m_hitmaps.at(hit), 16);
            }

            //TOT-map
            if (m_enable_tot) {
                for (uint32_t i = 0; i < 16; i++) {
                    if ((m_hitmaps.at(hit) >> i) & 0x1) {
                        pos = writeBytes(bytes, pos, 0xF, 4);
                    }
                }
            }
            //we are never first again
            if (isfirst) { isfirst = false; }
        }

        //end of stream
        if (m_tags.size() > 0) { pos = writeBytes(bytes, pos, 0, 6); }

        return pos;
    }


    uint32_t UnPack(uint8_t *bytes, uint32_t maxlen) {
        bool islast = 0;
        bool isneighbor = 0;
        uint32_t pre = 0;
        uint32_t tag = 0;
        uint32_t col = 0;
        uint32_t row = 0;
        uint32_t hit = 0;
        uint32_t tmphit = 0;
        uint64_t tot = 0;
        uint32_t pos = 0;
        uint32_t tpos = 0;
        uint32_t tcol = 0;
        uint32_t nbits = maxlen * 8;
        uint32_t NHits = 0;
        static const uint32_t tagsize = 8;
        static const uint32_t colsize = 6;
        static const uint32_t rowsize = 8;

        pos = readBytes(bytes, pos, tag, tagsize);
        m_tag = tag;
        tpos = readBytes(bytes, pos, tcol, colsize);
        if (tcol < 55) { //If it's a legit column
            col = tcol;
            pos = tpos;
        }
        while (pos < (nbits)) {
            if (islast) {
                //Check if there is
                uint32_t tcol = 0;
                tpos = readBytes(bytes, pos, tcol, colsize);
                if (tcol ==
                    0x0) { pos = tpos; /*cout << "BMDEBUG: tcol=0x0" << endl;*/ break; } //Collumn == 0 is a sign of end of stream
                if (tcol < 55) { //If it's a legit collumn
                    col = tcol;
                    pos = tpos;
                } else { //Else means there is a 11bit tag involvded that needs reconstruction
                    pos = readBytes(bytes, pos, pre, 3);
                    pos = readBytes(bytes, pos, tag, tagsize);
                    //cout << "BMDEBUG2: tag = " << tag << " --- " << ((tag&0xFC)>>2) << " , " <<  (tag&0x3) <<  endl;
                    m_tag = tag;
                    pos = readBytes(bytes, pos, col, colsize);
                }
            }
            //is-last
            pos = readBytes(bytes, pos, islast);
            //is-neighbor
            pos = readBytes(bytes, pos, isneighbor);
            //row
            if (!isneighbor) {
                pos = readBytes(bytes, pos, row, rowsize);
            } else {
                row += 1;
            }
            //hit-map
            if (m_enable_enc) {
                uint32_t map = 0;
                uint32_t nbs = 0;
                //Carlos hitmap
                readBytes(bytes, pos, map, 30);
                nbs = HitMap::decode(map, hit);
                if ((pos % 64) == 0) pos++;
                if (m_enable_cid && (pos % 64) == 1) pos++;
                if (m_enable_cid && (pos % 64) == 2) pos++;
                if ((pos % 64 + nbs) <= 64)
                    pos += nbs;
                else
                    pos += (nbs + (m_enable_cid ? 3 : 1));
            } else {
                pos = readBytes(bytes, pos, hit, 16);
            }
            NHits = 0;
            tmphit = hit;
            for (uint32_t ind = 0; ind < 16; ind++) {
                NHits += (tmphit & 0x1);
                tmphit = tmphit >> 1;
            }
            pos = readBytes(bytes, pos, tot, 4 * NHits);
        }
        return pos;
    }
private:
    bool m_enable_cid{true};
    bool m_enable_enc{true};
    bool m_enable_tot{true};
    std::vector<uint32_t> m_tags;
    std::vector<uint32_t> m_qcols;
    std::vector<uint32_t> m_qrows;
    std::vector<uint32_t> m_hitmaps;
    std::vector<uint64_t> m_totmaps;
    std::vector<uint32_t> m_islast;
    std::vector<uint32_t> m_isneigbor;
    uint32_t m_tag;

};
