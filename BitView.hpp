//
// Created by wittgen on 9/19/23.
//

#pragma once

#include <type_traits>
#include <cstdint>
#include <cassert>

namespace utils{
template<typename T>
[[gnu::always_inline]] static inline constexpr T bit_extract(T value, std::size_t start, std::size_t len)
requires(std::is_unsigned_v<T>) {
    T result;
    result = (value >> start) & ((T(1) << len) - 1);
    return result;
}
}

template<class T> concept UnsignedArray =
(sizeof(typename T::value_type) == 8)
&& requires(T a) {
    std::unsigned_integral<typename T::value_type>;
    std::contiguous_iterator<typename T::const_iterator>;
    a.size();
};

template<typename Buffer, std::size_t Offset> requires UnsignedArray<Buffer>
struct BitView {
    using value_type = typename Buffer::value_type;
    static constexpr std::size_t nBits = sizeof(value_type) * 8;
    static constexpr std::size_t bitLength = nBits - Offset;
    explicit BitView(Buffer const &data) :
            data(data),
            size((data.size() - 1) * nBits) {}

    [[gnu::always_inline]] uint64_t first64() {
        return data[0];
    }

    [[gnu::always_inline]] void set(std::size_t N) {
        index = N;
    }

    template <std::size_t N>
    [[gnu::always_inline]] inline bool fetch(value_type &value) {
        if (index + N > size) return false;
        auto i_word = index / nBits;
        auto i_bit = index % nBits;
        auto left = bitLength - i_bit;
        if(N <= left) {
            value = utils::bit_extract(data[i_word], bitLength - i_bit - N, N);
            index += N;
            return true;
        }
        auto w2 = N - left;
        auto word_1 = utils::bit_extract(data[i_word], bitLength - i_bit - left , left);
        auto word_2 = utils::bit_extract(data[i_word + 1], bitLength - w2 , w2);
        value = word_2 | (word_1 << w2);
        index += N + Offset;
        return true;
    }

    [[gnu::always_inline]] inline bool fetch(value_type &value, std::size_t N) {
        if (index + N > size) return false;
        auto i_word = index / nBits;
        auto i_bit = index % nBits;
        auto left = bitLength - i_bit;
        if(N <= left) {
            value = utils::bit_extract(data[i_word], bitLength - i_bit - N, N);
            index += N;
            return true;
        }
        auto w2 = N - left;
        auto word_1 = utils::bit_extract(data[i_word], bitLength - i_bit - left , left);
        auto word_2 = utils::bit_extract(data[i_word + 1], bitLength - w2 , w2);
        value = word_2 | (word_1 << w2);
        index += N + Offset;
        return true;
    }

    template <std::size_t N>
    [[gnu::always_inline]] inline bool prefetch(value_type &value) {
        if (index + N > size) return false;
        auto i_word = index / nBits;
        auto i_bit = index % nBits;
        auto left = bitLength - i_bit;
        if(N <= left) {
            value = utils::bit_extract(data[i_word], bitLength - i_bit - N, N);
            return true;
        }
        auto w2 = N - left;
        auto word_1 = utils::bit_extract(data[i_word], bitLength - i_bit - left , left);
        auto word_2 = utils::bit_extract(data[i_word + 1], bitLength - w2 , w2);
        value = word_2 | (word_1 << w2);
        return true;
    }

    [[gnu::always_inline]] inline bool prefetch(value_type &value, std::size_t N) {
        if (index + N > size) return false;
        auto i_word = index / nBits;
        auto i_bit = index % nBits;
        auto left = bitLength - i_bit;
        if(N <= left) {
            value = utils::bit_extract(data[i_word], bitLength - i_bit - N, N);
            return true;
        }
        auto w2 = N - left;
        auto word_1 = utils::bit_extract(data[i_word], bitLength - i_bit - left , left);
        auto word_2 = utils::bit_extract(data[i_word + 1], bitLength - w2 , w2);
        value = word_2 | (word_1 << w2);
        return true;
    }

    template <std::size_t N>
    [[gnu::always_inline]] inline void advance()
    {
        auto i_bit = index % nBits;
        auto left = bitLength - i_bit;
        if(N <= left) index += N;
        else index += (N + Offset);
    }

    [[gnu::always_inline]] inline void advance(std::size_t N)
    {
        auto i_bit = index % nBits;
        auto left = bitLength - i_bit;
        if(N <= left) index += N;
        else index += (N + Offset);
    }

    Buffer const &data;
    std::size_t index{0};
    const std::size_t size;
};

