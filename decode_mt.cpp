#include <iostream>
#include <memory>
#include <chrono>
#include <algorithm>
#include <execution>


#include "TestStream.hpp"
#include "Decoder.hpp"

static  std::vector<uint64_t> stream[8] {
        test_stream, test_stream,
        test_stream, test_stream,
        test_stream, test_stream,
        test_stream, test_stream
};
static uint64_t count[9];



//tf::Taskflow taskflow;
double  total = 0;
static void BM_stream_decode() {
    for (uint64_t n=0 ;n<1'0000;++n)  {
        //tf::Executor executor;
        uint64_t bits[8]{};
        std::vector<uint32_t> range{0, 1, 2,3};
        std::transform(std::execution::par_unseq, range.cbegin(), range.cend(), bits,
                       [](unsigned i) {
                           auto sum = 0;
                           for(unsigned k =0 ;k<128;k++) {
                           StreamDecoder< const decltype(test_stream)> decoder(stream[i]);
                           decoder.decode();
                           sum += decoder.bits_processed();
}
                           return sum;
                       });


        for(auto b: bits) total+=b;
    }
}

int main() {
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::duration;
    using std::chrono::milliseconds;
    auto t1 = high_resolution_clock::now();
    std::cout << "Start\n";
    BM_stream_decode();
    std::cout << "Stop\n";
    auto t2 = high_resolution_clock::now();
    duration<double, std::milli> ms_double = t2 - t1;


    std::cout << ms_double.count() << "ms\n";
    std::cout << "Total: " <<  total << "\n";
    std::cout << "rate: " << double(total) / (ms_double.count()/1000) /1000. /1000/1000.<< "\n";
}
