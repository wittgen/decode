#pragma once

#include <cstdint>
#include <memory>

template <bool UseLUT = false>
class HitMapImpl {
public:
    HitMapImpl() = default;
    ~HitMapImpl() = default;

    using Entry = struct Entry_{
            Entry_() = default;
            Entry_(uint16_t decoded_,uint8_t  length_ ):
            decoded(decoded_), length(length_) {}
            uint16_t decoded{};
            uint8_t length{};
    };
    static inline uint32_t encode(uint32_t decoded, uint32_t &encoded) {
        uint32_t Mbrr = ((decoded >> 14) & 0x1) << 1 | ((decoded >> 15) & 0x1);
        uint32_t Mbrl = ((decoded >> 12) & 0x1) << 1 | ((decoded >> 13) & 0x1);
        uint32_t Mblr = ((decoded >> 10) & 0x1) << 1 | ((decoded >> 11) & 0x1);
        uint32_t Mbll = ((decoded >> 8) & 0x1) << 1 | ((decoded >> 9) & 0x1);
        uint32_t Mtrr = ((decoded >> 6) & 0x1) << 1 | ((decoded >> 7) & 0x1);
        uint32_t Mtrl = ((decoded >> 4) & 0x1) << 1 | ((decoded >> 5) & 0x1);
        uint32_t Mtlr = ((decoded >> 2) & 0x1) << 1 | ((decoded >> 3) & 0x1);
        uint32_t Mtll = ((decoded >> 0) & 0x1) << 1 | ((decoded >> 1) & 0x1);
        uint32_t S1 = (bool(Mtll or Mtlr or Mtrl or Mtrr) << 1) | bool(Mbll or Mblr or Mbrl or Mbrr);
        uint32_t S2t = (bool(Mtll or Mtlr) << 1) | bool(Mtrl or Mtrr);
        uint32_t S2b = (bool(Mbll or Mblr) << 1) | bool(Mbrl or Mbrr);
        uint32_t S3tl = (bool(Mtll) << 1) | bool(Mtlr);
        uint32_t S3tr = (bool(Mtrl) << 1) | bool(Mtrr);
        uint32_t S3bl = (bool(Mbll) << 1) | bool(Mblr);
        uint32_t S3br = (bool(Mbrl) << 1) | bool(Mbrr);
        uint32_t pos = 0;
        encoded = 0;
        if (S1) writeTwo(S1, pos, encoded);
        if (S2t) writeTwo(S2t, pos, encoded);
        if (S3tl) writeTwo(S3tl, pos, encoded);
        if (S3tr) writeTwo(S3tr, pos, encoded);
        if (Mtll) writeTwo(Mtll, pos, encoded);
        if (Mtlr) writeTwo(Mtlr, pos, encoded);
        if (Mtrl) writeTwo(Mtrl, pos, encoded);
        if (Mtrr) writeTwo(Mtrr, pos, encoded);
        if (S2b) writeTwo(S2b, pos, encoded);
        if (S3bl) writeTwo(S3bl, pos, encoded);
        if (S3br) writeTwo(S3br, pos, encoded);
        if (Mbll) writeTwo(Mbll, pos, encoded);
        if (Mblr) writeTwo(Mblr, pos, encoded);
        if (Mbrl) writeTwo(Mbrl, pos, encoded);
        if (Mbrr) writeTwo(Mbrr, pos, encoded);
        return pos;
    }

    [[gnu::always_inline]] static inline uint32_t decode(uint32_t encoded, uint32_t &decoded) {
        if constexpr(UseLUT) {
            auto entry = LUT.get()[encoded];
            decoded = entry.decoded;
            return entry.length;
        } else return decode_(encoded, decoded);
    }
    [[gnu::always_inline]] static inline uint32_t decode_(uint32_t encoded, uint32_t &decoded) {
        uint32_t pos{};
        uint32_t S1{};
        uint32_t S2t{};
        uint32_t S3tl{};
        uint32_t S3tr{};
        uint32_t Mtll{};
        uint32_t Mtlr{};
        uint32_t Mtrl{};
        uint32_t Mtrr{};
        uint32_t S2b{};
        uint32_t S3bl{};
        uint32_t S3br{};
        uint32_t Mbll{};
        uint32_t Mblr{};
        uint32_t Mbrl{};
        uint32_t Mbrr{};
        readTwo(encoded, pos, S1);
        switch (S1) {
            case 0b10:
                readTwo(encoded, pos, S2t);
                switch (S2t) {
                    case 0b10:
                        readTwo(encoded, pos, S3tl);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3tr);
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3tl);
                        readTwo(encoded, pos, S3tr);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                }
                break;
            case 0b01:
                readTwo(encoded, pos, S2b);
                switch (S2b) {
                    case 0b10:
                        readTwo(encoded, pos, S3bl);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3br);
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3bl);
                        readTwo(encoded, pos, S3br);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                }
                break;
            case 0b11:
                readTwo(encoded, pos, S2t);
                switch (S2t) {
                    case 0b10:
                        readTwo(encoded, pos, S3tl);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3tr);
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3tl);
                        readTwo(encoded, pos, S3tr);
                        if (S3tl == 0b10) {
                            readTwo(encoded, pos, Mtll);
                        } else if (S3tl == 0b01) {
                            readTwo(encoded, pos, Mtlr);
                        } else if (S3tl == 0b11) {
                            readTwo(encoded, pos, Mtll);
                            readTwo(encoded, pos, Mtlr);
                        }
                        if (S3tr == 0b10) {
                            readTwo(encoded, pos, Mtrl);
                        } else if (S3tr == 0b01) {
                            readTwo(encoded, pos, Mtrr);
                        } else if (S3tr == 0b11) {
                            readTwo(encoded, pos, Mtrl);
                            readTwo(encoded, pos, Mtrr);
                        }
                        break;
                }
                readTwo(encoded, pos, S2b);
                switch (S2b) {
                    case 0b10:
                        readTwo(encoded, pos, S3bl);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        break;
                    case 0b01:
                        readTwo(encoded, pos, S3br);
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                    case 0b11:
                        readTwo(encoded, pos, S3bl);
                        readTwo(encoded, pos, S3br);
                        if (S3bl == 0b10) {
                            readTwo(encoded, pos, Mbll);
                        } else if (S3bl == 0b01) {
                            readTwo(encoded, pos, Mblr);
                        } else if (S3bl == 0b11) {
                            readTwo(encoded, pos, Mbll);
                            readTwo(encoded, pos, Mblr);
                        }
                        if (S3br == 0b10) {
                            readTwo(encoded, pos, Mbrl);
                        } else if (S3br == 0b01) {
                            readTwo(encoded, pos, Mbrr);
                        } else if (S3br == 0b11) {
                            readTwo(encoded, pos, Mbrl);
                            readTwo(encoded, pos, Mbrr);
                        }
                        break;
                }
                break;
        }

        //build the decoded
        decoded = 0;
        decoded |= ((Mtll >> 1) & 0x1) << 0;
        decoded |= ((Mtll >> 0) & 0x1) << 1;
        decoded |= ((Mtlr >> 1) & 0x1) << 2;
        decoded |= ((Mtlr >> 0) & 0x1) << 3;
        decoded |= ((Mtrl >> 1) & 0x1) << 4;
        decoded |= ((Mtrl >> 0) & 0x1) << 5;
        decoded |= ((Mtrr >> 1) & 0x1) << 6;
        decoded |= ((Mtrr >> 0) & 0x1) << 7;
        decoded |= ((Mbll >> 1) & 0x1) << 8;
        decoded |= ((Mbll >> 0) & 0x1) << 9;
        decoded |= ((Mblr >> 1) & 0x1) << 10;
        decoded |= ((Mblr >> 0) & 0x1) << 11;
        decoded |= ((Mbrl >> 1) & 0x1) << 12;
        decoded |= ((Mbrl >> 0) & 0x1) << 13;
        decoded |= ((Mbrr >> 1) & 0x1) << 14;
        decoded |= ((Mbrr >> 0) & 0x1) << 15;

        return pos;
    }
private:
    static inline void readTwo(uint32_t src, uint32_t &pos, uint32_t &dst) {
        uint32_t val = (src >> (28 - pos)) & 0x3;
        if (val == 0b00 or val == 0b01) {
            dst = 0b01;
            pos++;
        } else {
            dst = val;
            pos+=2;
        }
    }

    static inline void writeTwo(uint32_t src, uint32_t &pos, uint32_t &dst) {
        if (src == 0b01) {
            dst |= (0b0) << (28 - pos);
            pos++;
        } else {
            dst |= (src & 0x3) << (28 - pos);
            pos+=2;
        }
    }

    [[gnu::always_inline]] static inline constexpr Entry  *create_lut() {
        if constexpr(UseLUT) {
            auto *p = new Entry [1<<30];
            for(uint32_t i=0;i<(1<<30);i++) {
                uint32_t decoded{};
                auto len = decode_(i,decoded);
                p[i] = Entry(decoded&0xffff, len&0xff);
            }
            return p;
        } else return nullptr;
    }
    static inline std::unique_ptr<Entry> LUT{create_lut()};
};