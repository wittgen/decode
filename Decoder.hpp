#pragma once

#include <cstdint>
#include <type_traits>
#include <concepts>
#include <iostream>
#include <bit>
#include <bitset>

#include "BitView.hpp"
#include "hitmap.hpp"
static inline HitMapImpl<true> HitMap;

template<typename T>
concept DataBuffer = requires (T t, std::size_t s) {
{ t.size() } -> std::convertible_to<std::size_t>;
{ t[s] } -> std::same_as<uint64_t const &>;
};

template <DataBuffer T, bool EnableTag_ = true,
        bool EnableChipId_ = true,
        bool CompressedHitMap_ = true>
class StreamDecoder {
public:
    explicit StreamDecoder(T const &data_) : data(data_) {}
    ~StreamDecoder() = default;
    static inline constexpr bool EnableTag = EnableTag_;
    static inline constexpr bool EnableChipId= EnableChipId_;
    static inline constexpr bool CompressedHitMap = CompressedHitMap_;
void decode() {
    auto first = data.first64(); //63
    auto chip_id = (first >> 61) & bit_mask(2);
    auto tag = (first>>53) & bit_mask(8);
    auto ccol = (first>>47) & bit_mask(6);
    auto ctrl = (first>>45) & bit_mask(2);
    auto is_last = (ctrl&2) == 2;
    auto is_neighbour = (ctrl&1) == 1;
    auto qrow = (first>>37) & bit_mask(8);
    auto hit_map = (first>>7) & bit_mask(30);
    uint32_t decoded{};
    auto len = HitMap.decode(hit_map, decoded);
    auto hits = std::popcount(decoded);
    data.set(24+len);
    uint64_t tot;
    data.fetch(tot,hits*4);
    //std::cout << len << " " << tot << " " << std::dec << hits << " " << qrow  << " " << ccol <<"\n";
    auto do_loop = true;
    while(do_loop) {
        if(is_last) {
            data.template prefetch<11>(ccol);
            if((ccol>>8) == 0b111) {
                data.template advance<11>();
            } else {
                ccol>>=5;
                data.template advance<6>();
            }
        }
        do_loop = data.fetch(ctrl, 2);
        is_last = (ctrl&2) == 2;
        is_neighbour = (ctrl&1) == 1;
        if(!is_neighbour) {
            data.template fetch<8>(qrow);
        } else qrow++;
        data.template prefetch<30>(hit_map);
        if(!do_loop) break;
        len = HitMap.decode(hit_map, decoded);
        hits = std::popcount(decoded);
        data.advance(len);
        do_loop = data.fetch(tot,hits*4);
        //std::cout << len << " " << tot << " " << std::dec << hits << " " << qrow  << " " << ccol <<"\n";
    }
}
std::size_t bits_processed() const {
    return data.index;
}

private:
    static inline constexpr uint64_t bit_mask(std::size_t n) {return (uint64_t(1)<<n)-1;}
    static inline constexpr uint64_t chip_id_shift = EnableChipId_ ? 2 : 0;
    static inline constexpr uint64_t tag_shift = EnableTag_ ? 8 : 0;
    static inline constexpr uint64_t hitmap_bits = CompressedHitMap_ ? 30 : 16;
    BitView<T, 3> data;
};

template<class T> concept DataProcessor =
        requires (T a) {
           a.process();
        } && requires(T a, int x) {
          a.func(x);
        };

