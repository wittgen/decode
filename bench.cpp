//
// Created by Matthias Wittgen on 9/20/23.
//
#include <benchmark/benchmark.h>
#include <iostream>
#include "BitView.hpp"

static void BM_bit_read64(benchmark::State &state) {
    for (auto _: state) {
        for (unsigned i = 0; i < 1; i++) {
            std::vector<uint64_t> v(1000000);
            BitView<std::vector<uint64_t>, 1> bv(v);
            auto sum = 0;
            while (true) {
                uint64_t value{};
                auto result = bv.fetch(value, state.range(0));
                if (!result) break;

                sum += value;
                //bv.roll_back(4);
            }
            state.counters["Rate"] = benchmark::Counter(v.size()*8*8, benchmark::Counter::kIsIterationInvariantRate, benchmark::Counter::OneK::kIs1000);

            benchmark::DoNotOptimize(sum);
        }
    }
}


BENCHMARK(BM_bit_read64)->RangeMultiplier(2)->Range(1, 64)->Unit(benchmark::kMillisecond);
BENCHMARK_MAIN();
