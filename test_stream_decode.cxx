//
// Created by Matthias Wittgen on 11/6/23.
//
#include "Decoder.hpp"
#include "TestStream.hpp"
#include <cstdint>
#include <bit>
#include <array>
#include <iomanip>

int table(int argc, char *argv[]) {
    uint32_t decode;
    uint32_t hist[30][16]{};
    for(unsigned i = 0;i<(1<<30);i++) {
        auto len = HitMap.decode(i, decode);
        auto bits = std::popcount(decode);
        if(bits==1 && len ==4) {
            std::cout << len << std::hex << " " << i << " " << decode<<"\n";
       }
        hist[len-1][bits-1]++;
    }
    for(int x=0;x<30;x++)  {
        for(int y=0;y<16;y++)
            std::cout << std::setw(9) <<hist[x][y] << " ";
        std::cout << "\n";
    }
    auto const data = test_stream;
    StreamDecoder<decltype(data)> decoder(data);
    decoder.decode();
    return 0;
}

int main(int argc, char *argv[]) {
    StreamDecoder<const decltype(test_stream)> decoder(test_stream);
    decoder.decode();


 }