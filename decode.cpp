#include <iostream>
#include <memory>
#include "BitView.hpp"

#include "TestStream.hpp"
#include <benchmark/benchmark.h>
#include "Decoder.hpp"
static void BM_stream_decode(benchmark::State &state) {
    for (auto _: state) {
        StreamDecoder<const decltype(test_stream)> decoder(test_stream);
        decoder.decode();
        auto bits = decoder.bits_processed();
        benchmark::DoNotOptimize(bits);
        state.counters["Rate"] = benchmark::Counter(bits, benchmark::Counter::kIsIterationInvariantRate, benchmark::Counter::OneK::kIs1000);
    }
}

BENCHMARK(BM_stream_decode)->Unit(benchmark::kMillisecond);
BENCHMARK_MAIN();
